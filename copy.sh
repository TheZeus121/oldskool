#!/bin/sh

set -e

echo "Copying dependencies..."
cp node_modules/animate.css/animate.min.* bin/js/

cp node_modules/jquery/dist/jquery.slim.min.* bin/js/

cp node_modules/xp.css/dist/98.css* bin/js/
cp node_modules/xp.css/dist/ms_sans_serif* bin/js/
cp node_modules/xp.css/dist/PerfectDOSVGA437Win.woff* bin/js/

echo "Copying static resources..."
cp res/index.html bin/js/
cp res/boot.css bin/js/
cp res/style.css bin/js/
