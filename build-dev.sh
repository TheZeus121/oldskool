#!/bin/sh

set -e

# Haxe
echo "Compiling..."
haxe build.hxml

# License
cat res/license-header.js bin/js/main.js > bin/js/main.bloat.js
cat res/license-header.js bin/js/loader.js > bin/js/loader.bloat.js

mv bin/js/main.bloat.js bin/js/main.js
mv bin/js/loader.bloat.js bin/js/loader.js

./copy.sh
