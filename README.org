* oldskool
** Licensing
   Everything inside this repository is licensed under AGPL-3.0 or
   later version.  See file LICENSE for more information.
** Dependencies
   First install [[https://haxe.org][haxe]].  Then run the following in the project
   directory:
   #+BEGIN_SRC sh
     haxelib newrepo
     haxelib install all --always
   #+END_SRC

   There's also an optional dependency to Google's Closure compiler
   tool.  To install that do the following:
   #+BEGIN_SRC sh
     npm install
   #+END_SRC
** Building
   Minimal build:
   #+BEGIN_SRC sh
     haxe build.hxml
   #+END_SRC

   Build with closure:
   #+BEGIN_SRC sh
     ./build.sh
   #+END_SRC
