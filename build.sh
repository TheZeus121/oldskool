#!/bin/sh

set -e

# Haxe
echo "Compiling..."
haxe build.hxml

# License
cat res/license-header.js bin/js/main.js > bin/js/main.bloat.js
cat res/license-header.js bin/js/loader.js > bin/js/loader.bloat.js

# Closure
echo "Optimising..."
npx google-closure-compiler --js bin/js/main.bloat.js --js_output_file bin/js/main.js
rm bin/js/main.bloat.js

npx google-closure-compiler --js bin/js/loader.bloat.js --js_output_file bin/js/loader.js
rm bin/js/loader.bloat.js

./copy.sh
