#!/bin/sh

set -e

./build.sh
echo "Uploading..."

# Neocities doesn't like it when I upload map files
rm bin/js/*.map

neocities push bin/js/
