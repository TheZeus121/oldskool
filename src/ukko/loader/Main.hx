/* loader/Main.hx -- loads scripts, stylesheets asynchronously. -*- js -*-
 * Copyright (C) 2020  Uko Koknevics
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package ukko.loader;

class Main {
  static function main(): Void {
    final animate = new Dependency(
      "animate.css",
      new Style(),
      ["https://unpkg.com/animate.css@^4/animate.min.css",
       "/animate.min.css"]);

    final xp98 = new Dependency(
      "xp.css/98.css",
      new Style(),
      ["https://unpkg.com/xp.css@^0.2/dist/98.css",
       "/98.css"]);

    final style = new Dependency(
      "/style.css",
      new Style(),
      ["/style.css"]);

    style.addSoftRequirement(animate);

    final jquery = new Dependency(
      "jquery",
      new Script(),
      ["https://unpkg.com/jquery@3.4.1/dist/jquery.slim.min.js",
       "/jquery.slim.min.js"]);

    final mainjs = new Dependency(
      "/main.js",
      new Script(),
      ["/main.js"]);

    mainjs.addHardRequirement(jquery);
    mainjs.addHardRequirement(style);
    mainjs.addHardRequirement(xp98);

    // XXX: error processing
    mainjs.load((_) -> {});
  }
}
