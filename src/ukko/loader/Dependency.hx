/* loader/Dependency.hx -- Defines a dependency. -*- js -*-
 * Copyright (C) 2020  Uko Koknevics
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package ukko.loader;

import js.html.Element;
import js.Browser;

class Dependency {
  static var term = Browser.document.querySelector("pre.boot");

  private var name: String;
  private var type: DependencyType;
  private var sources: Array<String>;

  private var loaded = false;
  private var hardRequirements: Array<Dependency> = [];
  private var softRequirements: Array<Dependency> = [];

  public function new(name: String,
                      type: DependencyType,
                      sources: Array<String>) {
    this.name = name;
    this.type = type;
    this.sources = sources;
  }

  public function addHardRequirement(req: Dependency): Void {
    hardRequirements.push(req);
    loaded = false;
  }

  public function addSoftRequirement(req: Dependency): Void {
    softRequirements.push(req);
    loaded = false;
  }

  public function load(callback: (success: Bool) -> Void): Void {
    if (loaded) {
      callback(true);
    }

    final targetLoaded = hardRequirements.length + softRequirements.length;
    if (targetLoaded == 0) {
      return actuallyLoad(callback);
    }

    var loadedReqs = 0;

    function checkLoaded() {
      if (loadedReqs >= targetLoaded) {
        actuallyLoad(callback);
      }
    }

    for (hard in hardRequirements) {
      hard.load((success) -> if (success) {
        loadedReqs += 1;
        checkLoaded();
      } else {
        // XXX: BSOD
        term.append("\n[ERR] Couldn't load " + name
                    + " because of a missing hard dependency "
                    + hard.name + ".");
        callback(false);
      });
    }

    for (soft in softRequirements) {
      soft.load((_) -> {
        loadedReqs += 1;
        checkLoaded();
      });
    }
  }

  private function actuallyLoad(callback: Bool -> Void): Void {
    function trySource(idx: Int): Void {
      if (idx >= sources.length) {
        term.append("\n[ERR] Failed to load " + name);
        return callback(false);
      }

      term.append("\n      Loading " + name + " "
                  + Std.string(idx + 1) + "/"
                  + Std.string(sources.length) + "...");

      final el = type.makeElement(sources[idx]);
      el.onload = () -> {
        loaded = true;
        term.append("\n [OK] Loaded " + name);
        callback(true);
      };

      el.onerror = () -> trySource(idx + 1);
      Browser.document.head.appendChild(el);
    }

    trySource(0);
  }
}
