/* old/Main.hx -- entry point for the webpage. -*- js -*-
 * Copyright (C) 2020  Uko Koknevics
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package ukko.old;

import js.Browser;
import js.jquery.JQuery;

import ukko.old.JQueryAnimate.*;

import ukko.old.apps.About;
import ukko.old.apps.Temmie;

class Main {
  static function main(): Void {
    new JQuery(onload);
  }

  static function onload(): Void {
    new JQuery(".boot").remove();
    new JQuery("footer").show();
    new JQuery("main").show();

    Clock.start(new JQuery("time"));

    final start_button = new JQuery("#start-button");

    start_button.on("click.start-menu", start_handler);

    var start_menu_body = new JQuery("#start-menu-body");
    temmie_button().appendTo(start_menu_body);
    about_button().appendTo(start_menu_body);
  }

  private static function about_button(): JQuery {
    var button = new JQuery("<button>About</button>");
    button.on("click.about", () -> {
      new About().run();
    });

    return button;
  }

  private static function temmie_button(): JQuery {
    final button = new JQuery("<button>tEEMM</button>");
    button.on("click.tem", () -> {
      new Temmie().run();
    });

    return button;
  }

  private static function start_handler(): Void {
    final document     = new JQuery(Browser.document);
    final start_button = new JQuery("#start-button");
    final start_menu   = new JQuery("#start-menu");

    start_button.off("click.start-menu");

    start_menu.show();
    animate(start_menu, "slideInUp");

    document.on("click.start-menu", (evt) -> {
      var tgt = new JQuery(evt.target);
      if (!tgt.is("#start-menu")
          && !tgt.is("#start-button")
          && tgt.parents("#start-menu").length == 0
          && tgt.parents("#start-button").length == 0) {
        document.off("click.start-menu");
        start_button.on("click.start-menu", start_handler);
        animate(start_menu, "slideOutDown", start_menu.hide);
      }
    });
  }
}
