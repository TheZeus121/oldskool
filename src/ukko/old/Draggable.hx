/* Draggable.hx -- a class for draggable components. -*- js -*-
 * Copyright (C) 2020  Uko Koknevics
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package ukko.old;

import js.Browser;
import js.jquery.Event;
import js.jquery.JQuery;

class Draggable extends Stackable {
  private var handle: JQuery;

  public function new(html: JQuery, ?handle: JQuery) {
    super(html);

    if (handle != null) {
      this.handle = handle;
    } else {
      // TODO: allow specifying handle
      this.handle = html.children(".title-bar");
    }

    this.handle.on("mousedown.draggable", startDrag);

    // CSS
    this.handle.addClass("handle-inactive");
  }

  private function drag(evt: Event, rest: Dynamic): Void {
    evt.preventDefault();

    var pos = evt.data;

    var newPos = {
      x: evt.clientX,
      y: evt.clientY
    };

    var posDelta = {
      x: newPos.x - pos.x,
      y: newPos.y - pos.y
    };

    var margin = {
      x: Std.parseInt(html.css("margin-left")),
      y: Std.parseInt(html.css("margin-top"))
    };

    if (margin.x == null) {
      margin.x = 0;
    }

    if (margin.y == null) {
      margin.y = 0;
    }

    var winPos = html.position();

    html.css("left", (winPos.left + posDelta.x - margin.x) + "px");
    html.css("top",  (winPos.top  + posDelta.y - margin.y) + "px");

    var document = new JQuery(Browser.document);
    document.off("mousemove.draggable");
    document.on("mousemove.draggable", null, newPos, drag);
  }

  private function stopDrag(): Void {
    var document = new JQuery(Browser.document);
    document.off("mouseup.draggable");
    document.off("mousemove.draggable");

    // CSS
    var body = new JQuery("body");

    handle.addClass("handle-inactive");
    body.removeClass("handle-active");
  }

  private function startDrag(evt: Event): Void {
    bringFront();

    var target = new JQuery(evt.target);
    if (target.is("button")) {
      // Don't drag from buttons.
      return;
    }

    evt.preventDefault();

    var pos = {
      x: evt.clientX,
      y: evt.clientY
    };

    var document = new JQuery(Browser.document);
    document.on("mouseup.draggable", stopDrag);
    document.on("mousemove.draggable", null, pos, drag);

    // CSS
    var body = new JQuery("body");

    body.addClass("handle-active");
    handle.removeClass("handle-inactive");
  }
}
