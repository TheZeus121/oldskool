/* Stackable.hx -- a class for stackable components. -*- js -*-
 * Copyright (C) 2020  Uko Koknevics
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package ukko.old;

import js.jquery.JQuery;

class Stackable {
  // TODO: get a better data structure
  private static var stackables: Array<Stackable> = [];

  private var html: JQuery;
  private var isFront: Bool;

  public function new(html: JQuery) {
    this.html = html;
    this.isFront = true;

    this.bringFront();

    stackables.push(this);

    html.css("position", "absolute");
    html.on("mousedown.stackable", bringFront);
  }

  public function bringFront(): Stackable {
    var maxZIndex = 0;
    for (stackable in stackables) {
      var thisZIndex = stackable.getZIndex();

      stackable.html.css("z-index", thisZIndex);
      if (thisZIndex > maxZIndex) {
        maxZIndex = thisZIndex;
      }

      stackable.isFront = false;
    }

    this.html.css("z-index", maxZIndex + 1);
    this.isFront = true;

    return this;
  }

  public function close(): Void {
    stackables = stackables.filter((elem) -> elem != this);
    html.off("mousedown.stackable");
  }

  private function getZIndex(): Int {
    var tmp = Std.parseInt(this.html.css("z-index"));
    if (tmp == null) {
      return 0;
    } else {
      return tmp;
    }
  }
}
