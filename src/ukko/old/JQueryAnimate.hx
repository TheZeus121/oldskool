/* JQueryAnimate.hx -- Wrapping animate.css. -*- js -*-
 * Copyright © 2020  Uko Koknevics <perkontevs@gmail.com>
 *
 * This Program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This Program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package ukko.old;

import js.jquery.JQuery;

class JQueryAnimate {
  public static function animate(
      node: JQuery,
      style: String,
      ?callback: () -> Void): JQuery {
    if (node.hasClass("animate__animated")) {
      return node;
    }

    final css_class = "animate__" + style;
    node.addClass("animate__animated");
    node.addClass(css_class);
    node.one("animationend", () -> {
      if (callback != null) {
        callback();
      }

      node.removeClass(css_class);
      node.removeClass("animate__animated");
    });

    return node;
  }
}
