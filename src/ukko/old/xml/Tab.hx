/* xml/Tab.hx -- Tab component. -*- js-jsx -*-
 * Copyright (C) 2020  Uko Koknevics
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package ukko.old.xml;

import js.Browser;
import js.html.Element;

@:uiComp("tab")
class Tab extends Base {
  @:p(string) public var label: Null<String>;
  @:p(bool) public var opened: Bool = false;

  public function new(?parent: Base) {
    if (!Std.is(parent, Tablist)) {
      // XXX: there must be a way to check this statically.
      throw "<tab> may only be placed inside a <tablist>";
    }

    super(parent);
  }

  override public function render(): Element {
    throw "<tab> can't be rendered by itself, render a <tablist> instead.";
  }
}
