/* xml/Tablist.hx -- <tablist> component. -*- js-jsx -*-
 * Copyright (C) 2020  Uko Koknevics
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package ukko.old.xml;

import js.Browser;
import js.html.Element;

@:uiComp("tablist")
class Tablist extends Base {
  private static var counter = 0;

  override public function render(): Element {
    // XXX: check whether only one child is marked as opened.
    final tabs = Browser.document.createDivElement();

    final tablist = Browser.document.createMenuElement();
    // XXX: add aria-label
    tablist.setAttribute("role", "tablist");
    tabs.append(tablist);

    var buttons = [];
    var panels = [];

    for (child in this.getChildren()) {
      var child = cast(child, Tab);

      final id = "tab-" + counter;
      counter += 1;

      final button = Browser.document.createButtonElement();
      buttons.push(button);
      button.setAttribute("role", "tab");
      button.setAttribute("aria-controls", id);
      if (child.opened) {
        button.setAttribute("aria-selected", "true");
      }

      button.innerText = child.label;
      button.onclick = () -> {
        for (btn in buttons) {
          btn.setAttribute("aria-selected", "false");
        }

        button.setAttribute("aria-selected", "true");

        for (panel in panels) {
          if (panel.id == id) {
            panel.removeAttribute("hidden");
          } else {
            panel.setAttribute("hidden", "");
          }
        }
      };

      tablist.append(button);

      final panel = Browser.document.createDivElement();
      panels.push(panel);
      panel.setAttribute("role", "tabpanel");
      panel.setAttribute("id", id);
      if (!child.opened) {
        panel.setAttribute("hidden", "");
      }

      for (grandchild in child.getChildren()) {
        panel.append(grandchild.render());
      }

      tabs.append(panel);
    }

    return tabs;
  }
}
