/* xml/Button.hx -- Button component. -*- js-jsx -*-
 * Copyright (C) 2020  Uko Koknevics
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package ukko.old.xml;

import js.Browser;
import js.html.Element;
import js.html.MouseEvent;

@:uiComp("button")
class Button extends Base {
  @:p(string) public var label: Null<String>;

  public var click: Null<MouseEvent -> Void>;

  override public function render(): Element {
    final el = Browser.document.createButtonElement();
    el.innerText = this.label;
    el.onclick = click;

    return el;
  }
}
