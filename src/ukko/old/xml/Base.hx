/* xml/Base.hx -- Base component. -*- js-jsx -*-
 * Copyright (C) 2020  Uko Koknevics
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package ukko.old.xml;

import js.html.Node;
import js.html.Text;

@:uiComp("base")
class Base implements domkit.Model<Base> implements domkit.Object {
  public var children: Array<Base> = [];
  public var dom: Null<domkit.Properties<Base>>;
  public var parent: Null<Base>;

  public function getChildren() return children;

  public function new(?parent: Base) {
    if (parent != null) {
      this.parent = parent;
      this.parent.children.push(this);
    }
  }

  public function remove(): Void {
    if (this.parent != null) {
      this.parent.children.remove(this);
      this.parent = null;
    }
  }

  public function render(): Node {
    return new Text();
  }
}
