/* xml/Window.hx -- Window component. -*- js-jsx -*-
 * Copyright (C) 2020  Uko Koknevics
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package ukko.old.xml;

import js.Browser;
import js.html.Element;
import js.jquery.JQuery;

@:uiComp("window")
class Window extends Base {
  // This class feels pretty bad to me, I could technically remove the
  // render() because the default implementation returns an empty Text
  // node, but this allows me to have a "sub-window" inside another
  // window.

  @:p(string) public var title: Null<String>;

  override public function render(): Element {
    final titleBarText = Browser.document.createDivElement();
    titleBarText.classList.add("title-bar-text");
    titleBarText.innerText = this.title;

    final minimizeBtn = Browser.document.createButtonElement();
    minimizeBtn.classList.add("minimize");
    minimizeBtn.setAttribute("aria-label", "Minimize");

    final closeBtn = Browser.document.createButtonElement();
    closeBtn.classList.add("close");
    closeBtn.setAttribute("aria-label", "Close");

    final titleBarControls = Browser.document.createDivElement();
    titleBarControls.classList.add("title-bar-controls");
    titleBarControls.append(minimizeBtn, closeBtn);

    final titleBar = Browser.document.createDivElement();
    titleBar.classList.add("title-bar");
    titleBar.append(titleBarText, titleBarControls);

    final windowBody = Browser.document.createDivElement();
    windowBody.classList.add("window-body");
    for (child in this.getChildren()) {
      windowBody.append(child.render());
    }

    final window = Browser.document.createDivElement();
    window.classList.add("window");
    window.append(titleBar, windowBody);

    return window;
  }

  public function run() {
    final title = this.title == null ? "New Window" : this.title;
    final win = new ukko.old.Window(title);
    final cont = win.contents();
    for (child in this.getChildren()) {
      new JQuery(child.render()).appendTo(cont);
    }

    win.show();
  }
}
