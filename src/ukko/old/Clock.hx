/* Clock.hx -- Utility functions for running clocks. -*- js -*-
 * Copyright (C) 2020  Uko Koknevics
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package ukko.old;

import js.Browser;
import js.jquery.JQuery;

// TODO: when clicked on clock, open a clock window.

class Clock {
  private static function uiString(date: Date): String {
    return DateTools.format(date, "%R");
  }

  private static function isoString(date: Date): String {
    return DateTools.format(date, "%FT%R");
  }

  private static function setUI(date: Date, elem: JQuery): Void {
    elem.attr("datetime", isoString(date));
    elem.html(uiString(date));
  }

  public static function start(elem: JQuery): Void {
    function update() {
      var date = Date.now();
      setUI(date, elem);
    }

    update();
    Browser.window.setInterval(update, 1000);
  }
}
