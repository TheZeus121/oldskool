/* apps/Temmie.hx -- hOI!!! i'm tEMMIE!! -*- js-jsx -*-
 * Copyright (C) 2020  Uko Koknevics
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package ukko.old.apps;

import ukko.old.xml.Base;
import ukko.old.xml.Window;

class Temmie extends Window {
  static var SRC =
    <window id="win">
      <button id="btn" />
    </window>

  public function new(?parent: Base) {
    super(parent);

    initComponent();

    // We're doing it like this instead of specifying in the SRC,
    // because domkit expects all attributes to be syntactically valid
    // CSS.
    win.title = "hOI!!! i'm teMMMIE!!";

    btn.label = "hOI!!!";
    btn.click = _ -> new Temmie().run();
  }
}
