/* apps/About.hx -- The About Window. -*- js-jsx -*-
 * Copyright (C) 2020  Uko Koknevics
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package ukko.old.apps;

import ukko.old.xml.Base;
import ukko.old.xml.Window;

class About extends Window {
  static var SRC =
    <window title="About">
      <tablist>
        <tab label="TabA" opened>
          This is first tab
        </tab><tab label="TabB">
          This is second tab
        </tab><tab label="TabC">
          This is third tab
        </tab>
      </tablist>
    </window>

  public function new(?parent: Base) {
    super(parent);

    initComponent();
  }
}
