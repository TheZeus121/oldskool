/* Window.hx -- A class defining a window. -*- js -*-
 * Copyright (C) 2020  Uko Koknevics
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License
 * as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public
 * License along with this program.  If not, see
 * <https://www.gnu.org/licenses/>.
 */

package ukko.old;

import js.Browser;
import js.jquery.JQuery;

import ukko.old.JQueryAnimate.*;

class Window extends Draggable {
  private static var windowButtons: Array<JQuery> = [];
  private static var nextPos = {
    x: 32,
    y: 32
  };

  private var minimized: Bool = false;
  private var windowButton: JQuery;

  public function new(title: String) {
    var html = new JQuery('
<div class="window">
  <div class="title-bar">
    <div class="title-bar-text">
      ${title}
    </div>

    <div class="title-bar-controls">
       <button class="minimize" aria-label="Minimize"></button>
       <button class="close" aria-label="Close"></button>
    </div>
  </div>
  <div class="window-body">
  </div>
</div>');
    var main = new JQuery("main");
    html.hide();
    html.appendTo(main);

    var windowBar = new JQuery("#window-bar");
    windowButton = new JQuery('<button>${title}</button>');
    windowButton.hide();
    windowButton.appendTo(windowBar);
    windowButtons.push(windowButton);

    super(html);

    // Positioning code

    var winSize = {
      width:  html.innerWidth(),
      height: html.innerHeight()
    };

    var winBounds = {
      right:  nextPos.x + winSize.width,
      bottom: nextPos.y + winSize.height
    };

    var footer = new JQuery("footer");
    var mainBounds = {
      right:  Browser.window.innerWidth,
      bottom: Browser.window.innerHeight - footer.height()
    };

    if (winBounds.right > mainBounds.right) {
      // Out of bounds on the right side, jump over to the left.
      html.css("left", "32px");
      nextPos.x = 64;
    } else {
      html.css("left", nextPos.x + "px");
      nextPos.x += 32;
    }

    if (winBounds.bottom > mainBounds.bottom) {
      // Out of bounds on the bottom side, jump over to the top.
      html.css("top", "32px");
      nextPos.y = 64;
    } else {
      html.css("top", nextPos.y + "px");
      nextPos.y += 32;
    }

    var titleBar = html.children(".title-bar");

    var controls = titleBar.children(".title-bar-controls");

    var closeButton = controls.children(".close");
    var minimizeButton = controls.children(".minimize");

    // Callbacks
    closeButton.on("click.window", close);
    minimizeButton.on("click.window", minimizeToggle);
    windowButton.on("click.window", () -> {
      if (minimized || isFront) {
        minimizeToggle();
      }

      bringFront();
    });
  }

  public function contents(): JQuery {
    return html.children(".window-body");
  }

  public function show(): Window {
    minimized = false;

    html.show();
    windowButton.addClass("restored");
    windowButton.show();

    animate(html, "zoomIn");

    return this;
  }

  override public function bringFront(): Window {
    super.bringFront();
    for (button in windowButtons) {
      button.removeClass("active");
    }

    windowButton.addClass("active");

    return this;
  }

  override public function close(): Void {
    super.close();
    animate(html, "zoomOut", () -> {
      html.remove();
      windowButton.remove();
    });
  }

  public function minimize(): Window {
    minimized = true;
    windowButton.removeClass("restored");

    animate(html, "backOutDown", () -> html.hide());

    return this;
  }

  public function restore(): Window {
    minimized = false;
    windowButton.addClass("restored");
    html.show();
    bringFront();

    animate(html, "backInUp");

    return this;
  }

  public function minimizeToggle(): Window {
    if (minimized) {
      return restore();
    } else {
      return minimize();
    }
  }
}
